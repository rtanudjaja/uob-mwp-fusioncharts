import './App.css';
import SPLineChart from './module/FusionChart/SPLineChart';
import PieChart from './module/FusionChart/PieChart';

function App() {
  return (
    <div className="App">
      <center>
        <SPLineChart />
      </center>
      <br />
      <center>
        <PieChart />
      </center>
    </div>
  );
}

export default App;
