import React, { useState, useEffect } from 'react'
// Include the react-fusioncharts component
import ReactFC from "react-fusioncharts";

// Include the fusioncharts library
import FusionCharts from "fusioncharts";

// Include the chart type
import Column2D from "fusioncharts/fusioncharts.charts";

// Include the theme as fusion
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

// Adding the chart and theme as dependency to the core fusioncharts
ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme);

const SPLineChart = () => {
  const [chart, setChart] = useState({})
  const [initialAmount, setInitialAmount] = useState(100000);

  const chartConfigs = {
    type: "spline",
    width: "900",
    height: "500",
    dataFormat: "json",
    dataSource: {
      chart: {
        caption: "Balance by Year",
        yAxisName: "Value (SGD)",
        anchorradius: "5",
        plotToolText: "Balance in $label is <b>$dataValue</b>",
        showHoverEffect: "1",
        showvalues: "1",
        numberPrefix: "$",
        theme: "fusion",
        anchorBgColor: "#4169E1",
        paletteColors: "#4169E1"
      },
      annotations: {
        groups: [{
          id: "whatUHave-annotation",
          items: [{
            id: "whatUHave-circle",
            type: "circle",
            x: "$dataset.0.set.0.x+15",
            y: "$dataset.0.set.0.y-3",
            radius: "9",
            color: "#ffffff",
            border: "2",
            borderColor: "#777777"
          }, {
            id: "whatUHave-labelBG1",
            type: "rectangle",
            radius: "9",
            color: "#777777",
            x: "$dataset.0.set.0.x-35",
            y: "$dataset.0.set.0.y-25",
            tox: "$dataset.0.set.0.x+65",
            toy: "$dataset.0.set.0.y-75"
          }, {
            id: "whatUHave-labelBG2",
            type: "rectangle",
            color: "#ffffff",
            x: "$dataset.0.set.0.x-30",
            y: "$dataset.0.set.0.y-30",
            tox: "$dataset.0.set.0.x+60",
            toy: "$dataset.0.set.0.y-53"
          }, {
            id: "whatUHave-label",
            type: "text",
            text: `${"What you have"}`,
            fillcolor: "#ffffff",
            rotate: "90",
            x: "$dataset.0.set.0.x+15",
            y: "$dataset.0.set.0.y-62.5"
          }, {
            id: "whatUHave-value",
            type: "text",
            text: `${"$127.4K"}`,
            bold: "1",
            fillcolor: "#000000",
            rotate: "90",
            x: "$dataset.0.set.0.x+15",
            y: "$dataset.0.set.0.y-42.5"
          }
          ]
        }, {
          id: "event-annotation",
          items: [{
            id: "event-circle",
            type: "circle",
            x: "$canvasStartX+415",
            y: "$canvasEndY-5",
            radius: "9",
            color: "#ffffff",
            border: "2",
            borderColor: "#ff0000"
          }, {
            id: "event-labelBG1",
            type: "rectangle",
            radius: "9",
            color: "#ff0000",
            x: "$canvasStartX+365",
            y: "$canvasEndY-30",
            tox: "$canvasStartX+465",
            toy: "$canvasEndY-80"
          }, {
            id: "event-labelBG2",
            type: "rectangle",
            color: "#ffffff",
            x: "$canvasStartX+370",
            y: "$canvasEndY-35",
            tox: "$canvasStartX+460",
            toy: "$canvasEndY-58"
          }, {
            id: "event-label",
            type: "text",
            text: "Event ?",
            fillcolor: "#ffffff",
            rotate: "90",
            x: "$canvasStartX+415",
            y: "$canvasEndY-67.5"
          }, {
            id: "event-value",
            type: "text",
            text: "+$10.0K",
            bold: "1",
            fillcolor: "#000000",
            rotate: "90",
            x: "$canvasStartX+415",
            y: "$canvasEndY-47.5"
          }
          ]
        }, {
          id: "goal-annotation",
          items: [{
            id: "goal-circle",
            type: "circle",
            x: "$dataset.0.set.6.x",
            y: "$dataset.0.set.6.y",
            radius: "9",
            color: "#ffffff",
            border: "2",
            borderColor: "#4169E1"
          }, {
            id: "goal-labelBG1",
            type: "rectangle",
            radius: "9",
            color: "#4169E1",
            x: "$dataset.0.set.6.x-75",
            y: "$dataset.0.set.6.y-30",
            tox: "$dataset.0.set.6.x+75",
            toy: "$dataset.0.set.6.y-80"
          }, {
            id: "goal-labelBG2",
            type: "rectangle",
            color: "#ffffff",
            x: "$dataset.0.set.6.x-60",
            y: "$dataset.0.set.6.y-35",
            tox: "$dataset.0.set.6.x+60",
            toy: "$dataset.0.set.6.y-58"
          }, {
            id: "goal-label",
            type: "text",
            text: "Projected Amount",
            fillcolor: "#ffffff",
            rotate: "90",
            x: "$dataset.0.set.6.x",
            y: "$dataset.0.set.6.y-67.5"
          }, {
            id: "goal-value",
            type: "text",
            text: "$1.4M",
            bold: "1",
            fillcolor: "#000000",
            rotate: "90",
            x: "$dataset.0.set.6.x",
            y: "$dataset.0.set.6.y-47.5"
          }
          ]
        }, {
          id: "target-annotation",
          items: [{
            id: "target-circle",
            type: "circle",
            x: "$dataset.0.set.6.x",
            y: "$yaxis.label.4.starty-15",
            radius: "9",
            color: "#ffffff",
            border: "2",
            borderColor: "#228B22"
          }, {
            id: "target-labelBG1",
            type: "rectangle",
            radius: "9",
            color: "#228B22",
            x: "$dataset.0.set.6.x-75",
            y: "$yaxis.label.4.starty-30",
            tox: "$dataset.0.set.6.x+75",
            toy: "$yaxis.label.4.starty-80"
          }, {
            id: "target-labelBG2",
            type: "rectangle",
            color: "#ffffff",
            x: "$dataset.0.set.6.x-60",
            y: "$yaxis.label.4.starty-35",
            tox: "$dataset.0.set.6.x+60",
            toy: "$yaxis.label.4.starty-58"
          }, {
            id: "target-label",
            type: "text",
            text: "Goal Amount",
            fillcolor: "#ffffff",
            rotate: "90",
            x: "$dataset.0.set.6.x",
            y: "$yaxis.label.4.starty-67.5"
          }, {
            id: "target-value",
            type: "text",
            text: "$2.25M",
            bold: "1",
            fillcolor: "#000000",
            rotate: "90",
            x: "$dataset.0.set.6.x",
            y: "$yaxis.label.4.starty-47.5"
          }
          ]
        }]
      },
      data: [{
        label: "2020",
        showValue: "0",
        value: "100000.00"
      }, {
        label: "2025",
        showValue: "0",
        value: "245407.89"
      }, {
        label: "2030",
        showValue: "0",
        value: "413975.46"
      }, {
        label: "2035",
        showValue: "0",
        value: "609391.54"
      }, {
        label: "2040",
        showValue: "0",
        value: "835932.31"
      }, {
        label: "2045",
        showValue: "0",
        value: "1098555.13"
      }, {
        label: "2050",
        showValue: "0",
        value: "1403006.94"
      }, {
        "vline": "true",
        "lineposition": "0",
        "color": "#6baa01",
        "alpha": "50",
        "labelHAlign": "right",
        "label": "Retirement",
        "labelPosition": "0",
        "dashed": "1"
      }, {
        label: "2055",
        value: "",
        showValue: "0",
        showLabel: "0"
      }
      ],
      "trendlines": [
        {
          "line": [
            {
              "startvalue": "2250000",
              "color": "#ffffff",
              "valueOnRight": "1",
              "displayvalue": ""
            }
          ]
        }
      ]
    }
  };

  const calcFutureBalance = (amt, r, n, t, pmt) => {
    return amt * ((1 + r / n) ** (n * t)) + (pmt * (((1 + r / n) ** (n * t) - 1) / (r / n)))
  }

  const generateData = (amt) => {
    let i = 2020;
    let newData = [{
      label: i.toString(),
      showValue: "0",
      value: amt.toString()
    }];
    for (i = 2025; i <= 2050; i += 5) {
      newData = [...newData, {
        label: i.toString(),
        showValue: "0",
        value: calcFutureBalance(amt, 0.03, 12, i - 2020, 2000).toString()
      }]
    }
    return ([...newData, {
      "vline": "true",
      "lineposition": "0",
      "color": "#6baa01",
      "alpha": "50",
      "labelHAlign": "right",
      "label": "Retirement",
      "labelPosition": "0",
      "dashed": "1"
    }, {
      label: "2055",
      value: "",
      showValue: "0",
      showLabel: "0"
    }]);
  }

  const handleChange = (event) => {
    setInitialAmount(event.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if (chart) {
      let newData = generateData(initialAmount);
      chart.setJSONData({ ...chart.getJSONData(), data: newData });
      chart.annotations.clear();
    }
  }

  useEffect(() => {
    const dataPlotClick = (eventObj, dataObj) => {
      chart.annotations.clear();
      chart.annotations.addItem("pop-up", {
        id: "pop-up-labelBG1",
        type: "rectangle",
        radius: "9",
        color: "#4169E1",
        x: `$dataset.0.set.${dataObj.dataIndex}.x-50`,
        y: `$dataset.0.set.${dataObj.dataIndex}.y-30`,
        tox: `$dataset.0.set.${dataObj.dataIndex}.x+50`,
        toy: `$dataset.0.set.${dataObj.dataIndex}.y-80`,
      }, true);
      chart.annotations.addItem("pop-up", {
        id: "pop-up-labelBG2",
        type: "rectangle",
        color: "#ffffff",
        x: `$dataset.0.set.${dataObj.dataIndex}.x-45`,
        y: `$dataset.0.set.${dataObj.dataIndex}.y-35`,
        tox: `$dataset.0.set.${dataObj.dataIndex}.x+45`,
        toy: `$dataset.0.set.${dataObj.dataIndex}.y-58`,
      }, true);
      chart.annotations.addItem("pop-up", {
        id: "pop-up-label",
        type: "text",
        text: `${dataObj.categoryLabel}`,
        fillcolor: "#ffffff",
        rotate: "90",
        x: `$dataset.0.set.${dataObj.dataIndex}.x`,
        y: `$dataset.0.set.${dataObj.dataIndex}.y-67.5`,
      }, true);
      chart.annotations.addItem("pop-up", {
        id: "pop-up-value",
        type: "text",
        text: `${dataObj.displayValue}`,
        bold: "1",
        fillcolor: "#000000",
        rotate: "90",
        x: `$dataset.0.set.${dataObj.dataIndex}.x`,
        y: `$dataset.0.set.${dataObj.dataIndex}.y-47.5`,
      }, true);
    }

    const annotationClick = (eventObj, dataObj) => {
      if (dataObj.groupId === "pop-up") {
        let tempValue = dataObj.groupOptions.items.filter(n => n.id === "pop-up-value");
        let popUpDisplayVallue = tempValue ? tempValue[0].text : null;
        if (popUpDisplayVallue) {
          chart.annotations.addItem("annotationpop-up", {
            "id": "annotationpop-up-text",
            "type": "text",
            "text": `- Cash : ${popUpDisplayVallue} SGD <br>- SRS : $500K SGD <br>- Investments : $123K SGD <br>- Insurance : $500K SGD `,
            "vAlign": "middle",
            "align": "left",
            "fontSize": "15",
            "bold": "1",
            "bgColor": "#4169E1",
            "fillcolor": "#ffffff",
            "x": "$canvasStartX + 50",
            "y": "$canvasStartY + 15"
          }, true);
        }
      }
      if (dataObj.groupId === "whatUHave-annotation") {
        let tempValue = dataObj.groupOptions.items.filter(n => n.id === "whatUHave-value");
        let popUpDisplayVallue = tempValue ? tempValue[0].text : null;
        if (popUpDisplayVallue) {
          for (let i = 0; i < dataObj.groupOptions.items.length; i++) {
            chart.annotations.addItem("whatUHave-annotation", dataObj.groupOptions.items[i], true)
          }
          chart.annotations.addItem("annotationpop-up", {
            "id": "annotationpop-up-text",
            "type": "text",
            "text": `- Cash : ${popUpDisplayVallue} SGD <br>- SRS : $500K SGD <br>- Investments : $123K SGD <br>- Insurance : $500K SGD `,
            "vAlign": "middle",
            "align": "left",
            "fontSize": "15",
            "bold": "1",
            "bgColor": "#4169E1",
            "fillcolor": "#ffffff",
            "x": "$canvasStartX + 50",
            "y": "$canvasStartY + 15"
          }, true);
        }
      }
    }
    if (chart.args) {
      chart.addEventListener('dataplotClick', dataPlotClick);
      chart.addEventListener('annotationClick', annotationClick);
    }
    // Specify how to clean up after this effect:    
    return function cleanup() {
      if (chart.args) {
        chart.removeEventListener('dataplotClick', dataPlotClick);
        chart.removeEventListener('annotationClick', annotationClick);
      }
    };
  }, [chart]);

  const clearAnnotation = () => {
    chart.annotations.clear();
  }

  const renderComplete = (chart) => {
    setChart(chart);
  }

  const generateJSON = () => {
    console.log(chart.getJSONData());
  }

  return (
    <div>
      line chart
      <form onSubmit={handleSubmit}>
        <label>Name:<input type="text" value={initialAmount} onChange={handleChange} /></label>
        <input type="submit" value="Submit" />
      </form>
      <ReactFC {...chartConfigs} onRender={renderComplete} />
      <button onClick={clearAnnotation}>Clear Annotation</button>
      <button onClick={generateJSON}>Generate JSON</button>
    </div>
  )
}

export default SPLineChart
