import React, { useState, useEffect } from 'react'
// Include the react-fusioncharts component
import ReactFC from "react-fusioncharts";

// Include the fusioncharts library
import FusionCharts from "fusioncharts";

// Include the chart type
import Column2D from "fusioncharts/fusioncharts.charts";

// Include the theme as fusion
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

// Adding the chart and theme as dependency to the core fusioncharts
ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme);

const PieChart = () => {
  const [chart, setChart] = useState({})

  const chartConfigs = {
    type: "doughnut2d",
    width: "900",
    height: "500",
    dataFormat: "json",
    dataSource: {
      "chart": {
        "caption": "Customer Net Worth Distribution",
        "subcaption": "Latest updated: 15/03/2021",
        "showpercentvalues": "1",
        "showvalues": "1",
        "defaultcenterlabel": "NET WORTH",
        "aligncaptionwithcanvas": "0",
        "captionpadding": "0",
        "decimals": "2",
        "doughnutRadius": "60%",
        "plottooltext":
          "<b>$percentValue</b> of customer(s) net worth are in <b>$displayValue</b>",
        "centerlabel": "$displayValue: $$value SGD",
        "legendPosition": "right",
        "interactiveLegend": "1",
        "theme": "fusion"
      },
      "annotations": {},
      "data": [
        {
          "label": "Cash:<br>53,456.78 SGD",
          "value": "53456.78",
          "displayValue": "Cash"
        },
        {
          "label": "SRS:<br>50,000.00 SGD",
          "value": "50000",
          "displayValue": "SRS"
        },
        {
          "label": "Investments:<br>123,456.78 SGD",
          "value": "123456.78",
          "displayValue": "Investments"
        },
        {
          "label": "Insurance cash value:<br>50,000.00 SGD",
          "value": "50000",
          "displayValue": "Insurance cash value"
        }
      ]
    }
  };

  useEffect(() => {
    const dataPlotClick = (eventObj, dataObj) => {
      chart.annotations.clear();
      chart.annotations.addItem("annotationpop-up", {
        "id": "annotationpop-up-text",
        "type": "text",
        "text": `Detail Donut Chart<br><br>Name: ${dataObj.displayValue}<br>Value: ${dataObj.value}`,
        "vAlign": "middle",
        "align": "left",
        "fontSize": "15",
        "bold": "1",
        "bgColor": "#4169E1",
        "fillcolor": "#ffffff",
        "x": "$canvasStartX + 50",
        "y": "$canvasStartY + 15"
      }, true);
    }

    if (chart.args) {
      chart.addEventListener('dataplotClick', dataPlotClick);
    }
    // Specify how to clean up after this effect:    
    return function cleanup() {
      if (chart.args) {
        chart.removeEventListener('dataplotClick', dataPlotClick);
      }
    };
  }, [chart]);

  const renderComplete = (chart) => {
    setChart(chart);
  }

  const clearAnnotation = () => {
    chart.annotations.clear();
  }

  const generateJSON = () => {
    console.log(chart.getJSONData());
  }

  return (
    <div>
      pie chart
      <ReactFC {...chartConfigs} onRender={renderComplete} />
      <button onClick={clearAnnotation}>Clear Annotation</button>
      <button onClick={generateJSON}>Generate JSON</button>
    </div>
  )
}

export default PieChart
